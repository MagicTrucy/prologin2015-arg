from django.contrib import admin
from .models import Question, Answer, Faction, PortalController
from django.utils import timezone

# Register your models here.

class AnswerInLine(admin.TabularInline):
    model = Answer
    extra = 4

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'current_question', 'already_asked', 'times_asked')
    fieldsets = [
        ('Question', {'fields': ['question_text', 'current_question', 'already_asked', 'times_asked']}),
        ]
    inlines = [AnswerInLine]

class PortalControllerAdmin(admin.ModelAdmin):
    list_display = ('faction', 'control_start', 'control_end', 'capture_time')
        
#add portal duration
admin.site.register(Question, QuestionAdmin)
admin.site.register(Faction)
admin.site.register(PortalController, PortalControllerAdmin)
