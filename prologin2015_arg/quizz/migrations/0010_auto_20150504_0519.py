# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quizz', '0009_auto_20150503_1743'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portalcontroller',
            name='control_end',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='portalcontroller',
            name='control_start',
            field=models.FloatField(default=1430734773.5922225),
        ),
    ]
