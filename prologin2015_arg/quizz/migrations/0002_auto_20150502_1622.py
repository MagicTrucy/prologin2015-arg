# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quizz', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('answer_text', models.CharField(max_length=200)),
                ('good_answer', models.BooleanField(default=False)),
                ('question', models.ForeignKey(to='quizz.Question')),
            ],
        ),
        migrations.RemoveField(
            model_name='answers',
            name='question',
        ),
        migrations.DeleteModel(
            name='Answers',
        ),
    ]
