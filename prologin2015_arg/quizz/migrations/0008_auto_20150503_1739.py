# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quizz', '0007_question_times_asked'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portalcontroller',
            name='faction',
            field=models.ForeignKey(to='quizz.Faction', blank=True),
        ),
    ]
