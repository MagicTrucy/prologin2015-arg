# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quizz', '0003_auto_20150502_1635'),
    ]

    operations = [
        migrations.CreateModel(
            name='PortalController',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('control_start', models.DateTimeField(auto_now=True)),
                ('control_end', models.DateTimeField(blank=True)),
                ('faction', models.ForeignKey(blank=True, to='quizz.Question')),
            ],
        ),
    ]
