# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quizz', '0008_auto_20150503_1739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portalcontroller',
            name='control_end',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
