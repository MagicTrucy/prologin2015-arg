# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quizz', '0006_question_already_asked'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='times_asked',
            field=models.IntegerField(default=0),
        ),
    ]
