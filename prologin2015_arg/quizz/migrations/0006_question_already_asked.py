# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quizz', '0005_question_current_question'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='already_asked',
            field=models.BooleanField(default=False),
        ),
    ]
