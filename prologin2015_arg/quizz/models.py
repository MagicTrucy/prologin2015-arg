from django.db import models
from django.utils import timezone
import time

# Create your models here.

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    image = models.CharField(max_length=200, blank=True)
    current_question = models.BooleanField(default=False)
    already_asked = models.BooleanField(default=False)
    times_asked = models.IntegerField(default=0)
    def __str__(self):
        return self.question_text

class Answer(models.Model):
    question = models.ForeignKey(Question)
    answer_text = models.CharField(max_length=200)
    good_answer = models.BooleanField(default=False)
    def __str__(self):
        return self.answer_text

class Faction(models.Model):
    faction_name = models.CharField(max_length=200)
    color = models.CharField(max_length=9)
    icon = models.CharField(max_length=200, blank=True)
    def __str__(self):
        return self.faction_name

class PortalController(models.Model):
    faction = models.ForeignKey(Faction, blank=True)
    control_start = models.DateTimeField(blank=True)
    control_end = models.DateTimeField(blank=True, null=True)
    def __str__(self):
        return self.faction.faction_name
    def capture_time(self):
        if self.control_end is not None:
            capture_duration = self.control_end - self.control_start
            return int(capture_duration.total_seconds() / 60)
        else:
            return "Current capture"

