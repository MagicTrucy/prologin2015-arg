from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^hack$', views.hack, name='hack'),
    url(r'^capture/(?P<faction_id>[0-9]+)$', views.capture, name='capture'),
]
