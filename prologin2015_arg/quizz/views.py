from django.shortcuts import render, redirect
from .models import Question, Answer, Faction, PortalController
import time
#from django.db.models import Max
from django.utils import timezone
from datetime import datetime
import random

# Create your views here.

def get_current_faction():
    last_controller = PortalController.objects.all().order_by('control_start').reverse()
    if len(last_controller) > 0:
        faction = Faction.objects.get(id=last_controller.faction.id)
    else:
        faction = Faction("Shapers", "#CACACA")
    return faction

def index(request):
    # if the portal has been hacked less than 5 minutes before, lock it
    now = timezone.now()
    captures = PortalController.objects.all().order_by('control_start').reverse()
    # If the portal was already captured (ie not at the beggining of the game)
    if len(captures) > 0:
        last_capture = captures[0]
        if (now - last_capture.control_start).total_seconds() / 60 <= 5:
            faction_color = last_capture.faction.color
            context = {'faction_color': faction_color}
            return render(request, 'quizz/locked.html', context)
        elif (now - last_capture.control_start).total_seconds() / 60 >= 15:
            last_capture.control_end = now
            last_capture.save()
    question_list = Question.objects.filter(current_question=True)
    if len(question_list) > 1:
        question = random.choice(question_list)
    else:
        question = question_list[0]
    faction_color = "#CACACA"
    if len(captures) > 0:
        last_capture = captures[0]
        if last_capture.control_end is None:
            faction_color = last_capture.faction.color
        else:
            faction_color = "#CACACA"
    context = {'question': question, 'faction_color': faction_color}
    return render(request, 'quizz/index.html', context)

def hack(request):
    question = Question.objects.get(id=request.POST['question_id'])
    if 'answer' not in request.POST:
        return redirect('index')
    answer = Answer.objects.get(id=request.POST['answer'])
    if answer.question.id == question.id and answer.good_answer:
        factions = Faction.objects.all()
        context = {'success': True, 'factions': factions}
    else:
        context = {'success': False}
    if context['success']:
        # Get only never asked questions
        question.already_asked = True
        question.current_question = False
        question.times_asked += 1
        question.save()
        available_questions = Question.objects.filter(already_asked=False)
        # If all questions have been asked before
        if len(available_questions) == 0:
            all_questions = Question.objects.all()
            for unit_question in all_questions:
                unit_question.already_asked = False
                unit_question.save()
            available_questions = all_questions
        next_question = random.choice(available_questions)
        next_question.current_question = True
        next_question.save()
    return render(request, 'quizz/hack.html', context)

def capture(request, faction_id):
    now = timezone.now()
    faction = Faction.objects.get(id=faction_id)
    captures = PortalController.objects.all().order_by('control_start').reverse()
    # If the portal was already captured (ie not at the beggining of the game)
    if len(captures) > 0:
        last_capture = captures[0]
        if (now - last_capture.control_start).total_seconds() / 60 >= 5:
            last_capture.control_end = now
            last_capture.save()
        else:
            return render(request, 'quizz/locked.html')
    new_capture = PortalController(faction=faction, control_start=now)
    new_capture.save()
    return render(request, 'quizz/capture.html')
